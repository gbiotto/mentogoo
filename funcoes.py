import streamlit as st
from bs4 import BeautifulSoup
from multiprocessing import Queue
from urllib.request import Request, urlopen
import random
import re
import pytest

class MentoGoo:
  def Function(lista_url):
    try:
      for url in lista_url:
        try:
          req = Request(url)
          html_pagina = urlopen(req)
          bs = BeautifulSoup(html_pagina, "html.parser")
        except:
          continue
        else:
          links = []
          for link in bs.findAll('a'):
            links.append(link.get('href'))
        lista_final = []
        for link in links:
          try:
            if re.findall("^https", link):
              lista_final.append(link)
          except:
            continue
        return lista_final
    except:
      return []
    else:
      return []

  def Recomendacao(lista):
    lista_final = random.choices(lista, k=20)
    fila = Queue()
    [fila.put(n) for n in lista_final]
    while not fila.empty():
      try:
        req = Request(fila.get())
        pagina_req = urlopen(req)
        pagina = BeautifulSoup(pagina_req, "html.parser").find('title')
        st.write(f'Pagina : {pagina.text}')
        st.write(f'Link : {fila.get()}')
        st.write('#############')
      except:
        pass
