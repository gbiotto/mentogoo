import io
from starlette.responses import StreamingResponse
import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel
from typing import Optional
import streamlit as st
from funcoes import MentoGoo

app = FastAPI()

st.set_page_config(
    page_title="MentoGoo",
    page_icon="images\logomentorama.jpg",
)

@app.get("/")
def mento_goo():
    st.image("images\MentoGoo.png", width = 300)

    try:
        url = st.text_input('Entre com a URL para realizar a busca')
    except:
        st.error('Digite uma URL valida')
    else:
        try:
            data_1 = MentoGoo.Function([url])
            data_2 = MentoGoo.Function(data_1)
            data_3 = MentoGoo.Function(data_2)
            data_4 = MentoGoo.Function(data_3)
            data_5 = MentoGoo.Function(data_4)
            data_6 = MentoGoo.Function(data_5)
            if type(data_1) is None:
                data_1 = [url]
            if type(data_2) is None:
                data_2 = [url]
            if type(data_3) is None:
                data_3 = [url]
            if type(data_4) is None:
                data_4 = [url]
            if type(data_5) is None:
                data_5 = [url]
            if type(data_6) is None:
                data_6 = [url]
            final = data_1 + data_2 + data_3 + data_4 + data_5 + data_6
            MentoGoo.Recomendacao(final)
        except:
            st.error('Digite um URL valida')

mento_goo()
